#### 问题描述

​	**环境：**VMware Fusion + kali-linux-2018.1-amd64.iso

​	**问题：**在Terminal利用`./pwn1`执行pwn1会出现 `bash： ./pwn1:没有那个文件或目录`的提示，但是`ls`命令又能看到存在pwn1文件。*<u>（下图中20154312为pwn1的一个副本）</u>*
![输入图片说明](https://gitee.com/uploads/images/2018/0306/185835_72401e34_1029509.png "屏幕快照 2018-03-05 下午6.55.44.png")


#### 问题解决

​	具体的问题分析过程我会附录在解决方案的后面，这里先把解决方案放出来。

#### 一.修改更新源sources.list

​	1.切换到root用户（大家如果按部就班地安装的话都是root用户）

```
kali@20154312:~$ su
密码：
```

​	2.用文本编辑器打开source.list

```ter
root@20154312: leafpad /etc/apt/sources.list
```

![输入图片说明](https://gitee.com/uploads/images/2018/0306/190023_72f9e965_1029509.png "屏幕快照 2018-03-05 下午8.46.24.png")



​	3.添加下列更新源

```
#阿里云kali源  
deb http://mirrors.aliyun.com/kali kali-rolling main non-free contrib  
deb-src http://mirrors.aliyun.com/kali kali-rolling main non-free contrib  
deb http://mirrors.aliyun.com/kali-security kali-rolling/updates main contrib non-free  
deb-src http://mirrors.aliyun.com/kali-security kali-rolling/updates main contrib non-free  
  
#中科大kali源  
deb http://mirrors.ustc.edu.cn/kali kali-rolling main non-free contrib  
deb-src http://mirrors.ustc.edu.cn/kali kali-rolling main non-free contrib  
deb http://mirrors.ustc.edu.cn/kali-security kali-current/updates main contrib non-free  
deb-src http://mirrors.ustc.edu.cn/kali-security kali-current/updates main contrib non-free  
```

​	4.对软件进行一次整体更新（一共923M的更新包）

```
apt-get clean
apt-get update
apt-get upgrade
```

#### 二.安装32位运行库

​	我使用的是第一个`lib32ncurses5`

```
apt-get install lib32ncurses5
```

​		或者

```
apt-get install lib32z1
```

![输入图片说明](https://gitee.com/uploads/images/2018/0306/190326_51da165a_1029509.png "屏幕快照 2018-03-05 下午7.50.42.png")


​	到这一步就已经可以正常使用`./pwn1`命令执行pwn1了

### 写在后面

​	安装完kali原以为万事大吉了，谁想到好不容易把VMwareTools折腾好了，又卡在了运行pwn1上。前面说了之所以没发正常使用`./pwn1`命令执行pwn1是因为这个64位的kali没有32位运行库。本来只要简单的`apt-get install ia32-libs`就可以了，但是这个kali本身的sources.list更新源极其有限且不适用于国内，所以就想到了添加更新源，中科大和阿里云的kali源都是比较好用的，所以选了这两个。

​	更新完kali源之后再次使用`apt-get install ia32-libs`结果发现`ia32-libs`这个软件包已经被废弃了，好在给了替代软件包的名字，再利用`apt-get install`命令就正确安装了32位的运行库。

**谢谢阅读，水平有限，如有错误之处还望批评指正。**